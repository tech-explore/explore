---
layout: post
title: Running your first simple multi container application in ec2
categories: [technical, containerization]
tags: [technical, docker, docker-compose, aws, ec2]
description: In this exercise, we will create a simple web application running on Node js and use Redis to keep a track of the visitor count. We will deploy and run the web and Redis services in two docker containers using docker-compose on an ec2 instance. The purpose of this tutorial is to understand basic concepts behind docker and docker-compose. 
comments: true
---

### Overview
<hr>

The aim of this article is to explain how Docker and docker-compose work and come-up with a working solution (application with multiple containers) to demonstrate Docker functionalities. We will use **Docker** and **docker-compose** to create and manage these containers and also handle the **external traffic** as well as **internal integration** between services. 

If you want to run containers in AWS, there are easier ways such as AWS Fargate, Beanstalk or EKS but the purpose of this article is to understand how Docker and Docker-compose work so we will be installing and configuring everything from scratch.
<br>
<br>

#### What are we trying to create?
<br>
In this article, we will create a simple **multi container application** and deploy it to a **Linux ec2 instance**. We will create a single page web application which returns a welcome message to the user and keeps a track of the number of visits to that page. The web page will be developed using **Java script and Node js** and we will use **Redis** as a caching server to keep a track of the visitor count.

To implement this solution, we will create **two containers, one for the web page and another one for the Redis server**. It is technically possible to deploy both these services to a single container but that is not the best practice. One major advantage of running the web UI and Redis in separate containers is, we can scale each service independently. For example, if we get large number of requests to the web site, we can increase the number of web UI containers to handle the traffic increase without making any changes to Redis. 

![Multi-container Architecture]({{site.BASE_PATH}}/assets/media/multi_container_app/Figure_1.png)

<br>
#### Pre-requisites

+ Basic understanding of Docker
+ Basic understanding of AWS, ec2 instances and security groups

<br>
### Getting your infrastructure ready
<hr>

#### Creating an ec2 instance

1. Login to AWS and navigate to ec2 console
2. Click on “Launch Instances”
3. For this exercise I’m using Amazon Linux 2 AMI (which is also free tier eligible)
<br>&nbsp;
   ![Select Instace Type]({{ site.BASE_PATH }}/assets/media/multi_container_app/Select_AMI_Type.png)
<br>&nbsp;
4. Select the instance type and click “Configure instance details”. For this exercise I have selected “t2.micro”.
5. Leave everything default and click “Add Storage”
6. Leave everything default and click “Add Tags”
7. Leave everything default and click “Configure Security groups”
8. Create a new security group and give a suitable name. For the moment we can keep the rules as it is but later we will be adding more rules to this security group.
<br>&nbsp;
   ![Create Security Group]({{ site.BASE_PATH }}/assets/media/multi_container_app/Create_Sec_Group.png)
<br>&nbsp;
9. Review and launch your instance
10. Choose an existing key pair which you have access to or create a new one. You will need this when you try access the instance.
11. Go back to the ec2 main page and wait while it completes the instance creation. Once the instance is ready, click the instance you just created.
12. There should be a button called “connect”
13. Click the third tab “SSH Client”. This UI gives instructions on how to ssh into your new instance using the key pair you selected at the time of creating the instance. 

<br>&nbsp;
#### Installing Docker

1. In order to get our instance to run Docker containers, first, we need to install docker in this instance
2. Use the following commands to install Docker
	```console
	sudo yum -y update
	sudo yum install -y docker
	```

3.  You can check whether Docker was successfully installed by running the following command. If you can see the docker version, then you are ready to proceed to the next step

	```console
	docker version or docker -v
	```
<br>&nbsp;
![Docker Version Info]({{ site.BASE_PATH }}/assets/media/multi_container_app/Docker_Version_Info.png)
<br>&nbsp;

4. The next step is to start the Docker service

	```console
	sudo service docker start
	```

5.  docker info command lets you check whether docker started successfully and also give the current status of docker. In the below screenshot you can see that there are 0 containers and there are no images in our instance. 

	```console
	sudo docker info
	```
<br>&nbsp;
![Docker Info]({{ site.BASE_PATH }}/assets/media/multi_container_app/Docker_Info.png)
<br>&nbsp;

6.  In order to prevent the use of sudo every time, you can add the current user to the docker group

	```console
	sudo usermod -a -G docker ec2-user
	```

7.  For the permissions to work, you have to logout from the instance and log back in. Once you do that, you should be able to run docker commands without sudo. To test whether the permissions were added successfully, type the following command

	```console
	docker info
	```

8.  Now we should be able to store images and run containers in your ec2 instance

<br>&nbsp;
#### Installing docker-compose

1. In order to run multiple containers and configure them to talk to each other, the easiest way is to use docker compose. Docker-compose gets automatically installed when you install docker in Windows or Mac, but in Linux, you have to install docker-compose separately.

2. Type the following command to check whether docker-compose is installed and if the command is not recognized, follow the next steps to install docker-compose.

   ```console
   docker-compose -v or docker-compose -version
   ```

   Latest Docker-compose releases can be accessed in Github. 

   https://github.com/docker/compose/releases

   I will be installing version 1.26.1 which is the current stable version at the time of writing this article. You might have to modify the following curl command if you want to install a newer version.

3. To install docker-compose, run the following commands in the terminal.

   ```console
   sudo yum -y update
   curl -L https://github.com/docker/compose/releases/download/1.26.1/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
   ```

4. Now to grant permissions, run the following command.

	```console
	chmod +x /usr/local/bin/docker-compose
	```

5. To test whether docker-compose is installed successfully, you can run the following command. If successfully installed, you should see the version and build number.

    ```console
    docker-compose -v or docker-compose -version
    ```

### Development 
<hr>

In this sections we will be creating a light weight java scrip application which can be executed in node js. We will be writing the code as well as configuration files which is needed for docker and docker-compose to successfully create and start the containers. 


1. ssh into your ec2 instance
2. Create a directory called **app** and go into the app directory. This will be your current working space.

	```console
	mkdir app
	cd app/
	```

3. Our first step is to create the java script code which would return the welcome message and the visitor count. Create a file called **index.js** and add the following code.

	```javascript
	//Declare the dependencies 
	const express = require('express');
	const redis = require('redis');
	const process = require('process');

	//Instantiate express
	const app = express();

	//Instantiate the Redis client
	const client = redis.createClient({
		host: 'redis-server',
		port: 6379
	});

	//Set the value to 0 in redis
	client.set('visits',0);

	//Return the welcome message and visits count 
	//Increment the variable by one
	//Set the new value in redis
	app.get('/', (req, res) => {
		client.get('visits', (err, visits) => {
			res.send('Hi visitor!, Number of visits are: ' + visits);
			client.set('visits', parseInt(visits) + 1);
		});
	});

	//Set the port
	app.listen(8081, () => {
		console.log('Listening on port 8081');
	});
	```

	Note: Usually when we instantiate a Redis client in the code, we either assign the IP or the DNS (preferably) of the Redis server as the host value (ex: ``host: 'myredis.mydomain.com'``. However, in this case, I have only given a string as ``host: 'redis-server'``. This will be explained in the upcoming sections.

### Configuration 
<hr>

We start the application (services, containers, etc.) using docker-compose. When we give the start command to docker-compose, docker-compose will work with Docker service to spin-up and deploy containers and also manage the communications between containers. 

1. The file which we give instructions to docker-compose is **docker-compose.yml**. In this file we are configuring two services "redis-server" and "node-app". Create a new file with the named **docker-compose.yml** and enter the following command

	```yml
	version: '3'
	services: 
		redis-server: 
			image: 'redis'
		node-app:
			build: .
			ports:
				- "4001:8081"
	```

	* For the first service, docker will check whether there is an image available called "redis" locally and if not, it will download the "redis" image from dockerhub (See this [link](https://hub.docker.com/_/redis) for more information). Also, **``redis-server`` is the same name we gave in the index.js file.** This is important because ``redis-server`` is the name tag which docker-compose use to route traffic.
	
	* For the second service we have not specified an image. Instead we are building a new container. Because we use ``build: .``, Docker will look for a file called **Dockerfile** for build instructions. We will be creating the Dockerfile later in this article. Also, in this section we are setting the port mapping for that container. By setting ``4001:8081``, we are instructing Docker to forward all traffic which comes to the 4001 port in the base machine to 8081 port in the container. Note that in our code index.js file, we set the port to 8081 (``app.listen(8081, () => {``)

2. Next, we write the instructions to docker to build an image for our node js container which will host the web UI. Create a file called **Dockerfile**. Note that this file name cannot change, there is no extension to the file and it is case sensitive.

	```yml
	 FROM node:alpine

	 WORKDIR '/app'

	 COPY package.json .
	 RUN npm install
	 COPY . .

	 CMD ["npm","start"]
	```

	* In the Dockerfile, first we set the image to node (alpine is the version). Docker will check whether there is an image available called "node:alpine" locally and if not, it will download the image from dockerhub. See this [link](https://hub.docker.com/_/node) for more information. The next steps of this file will be applied to a container created by using this image.
	* Next we create a directory in the container called 'app'. This will be the directory which we deploy our application and it's dependencies.
	* Then we start the process of installing the dependencies and copying the src files to the container. First, copy the package.json file from our current working directory to the app directory in the container (the package.json file will be created in the next step).
	* Next we instruct Docker to run ``npm install`` command in the container. Npm stands for Node Package Manager. Npm install downloads dependencies defined in the **package.json** file and generates a node_modules folder with the installed modules inside the container.
	* Once that is done, we copy all the rest of the files (such as **index.js**) from current working directory to app directory in the container.
	* Finally, we set the start up command of this container to ``npm start`` so that when we run the container npm will start automatically.

3. Next step is to create the configuration file which is needed for application properties. In this file we define what are the dependencies which needs to be installed in the container and the script file which needs to be triggered. Create a file named **package.json** and enter the following text.

	```json
	 {
	 	 "dependencies": {
	 		 "express": "*",
	 		 "redis": "2.8.0"
	 	 },
	 	 "scripts": {
	 		 "start": "node index.js"
	 	 }
	 }
	```

	Note: In this file I have given express (latest version) and Redis (version 2.8.0) as dependencies. As discussed earlier, ``npm install`` will use **package.json** file to install all dependencies in the container. The stating script we are instructing node to run the index.js file which we created in the previous step.

### Run! 
<hr>

At this point we have completed the development and configuration activities and finally it's time for us to the up the environment. If you have everything configured, running the environment is really simple. This simplicity comes from docker-compose. Instead of running each container separately and worrying about network connectivity and integrations, we use docker-compose to handle that complexity. 

Run the following command to up the environment.

```console
docker-compose up
```

If everything goes well, you should see the log output of each step we configured in your terminal. Let's try to understand what this log means. Please note that I have not added the full terminal output in each step to maintain simplicity.
<br>&nbsp;
#### Understanding Terminal output

1. In your terminal the first output should be from docker-compose creating a network for contaners
	```
	Creating network "app_default" with the default driver
	```

2. As discussed earlier, docker-compose will download redis images if it does not exist locally (first service in **docker-compose** file)

	```
	Pulling redis-server (redis:)...
	latest: Pulling from library/redis
	bb79b6b2107f: Pull complete
	1ed3521a5dcb: Pull complete
	069e700bc397: Pull complete
	Digest: sha256:a0494b60a0bc6de161d26dc2d2f9d2f1c5435e86a9e5d48862a161131affa6bd
	Status: Downloaded newer image for redis:latest
	```

3. The second service we defined in the **docker-compose** file is ``node-app``. As discussed, node image will be downloaded (if it does not exist locally), and docker will start to build a new image based on steps specified in **Dockerfile** 

	* Download ``node-alpine`` image from github
	   
	    ```
	    Building node-app
		Step 1/6 : FROM node:alpine
		alpine: Pulling from library/node
		cbdbe7a5bc2a: Pull complete
		c36334c36d30: Pull complete
		dc731be5fee1: Pull complete
		8a23f73dde1a: Pull complete
		Digest: sha256:90dfaf5806a607e81cc81b42b474626612c9271eee7a4fc6644ce6299348425a
		Status: Downloaded newer image for node:alpine
		---> 1e8b781248bb
	    ```
	* Create a directory called 'app'
		```
		Step 2/6 : WORKDIR '/app'
		---> Running in 5db36c2b8af6
		Removing intermediate container 5db36c2b8af6
		---> 6efd037c6c87
		```
	* Copy the **package.json** from base directory to the container
		```
		Step 3/6 : COPY package.json .
		---> 98075f6043c8
		```
	* Run ``npm-install`` to install dependencies needed to run the app
		```
		Step 4/6 : RUN npm install
		---> Running in 8bf750b0f916
		added 54 packages, and audited 54 packages in 3s
		Removing intermediate container 8bf750b0f916
 		---> 811349714601
		```
	* Copy rest of the files to the container
		```
		Step 5/6 : COPY . .
		---> 9c4030778006
		```
	* Start the node service and finish the build process
		```
		Step 6/6 : CMD ["npm","start"]
		---> Running in e8d642caf88e
		Removing intermediate container e8d642caf88e
		---> 51e433d1869b
		Successfully built 51e433d1869b
		Successfully tagged app_node-app:latest
		```
	* If both images were built successfully, you should see the following output in your terminal
		```
		Creating app_redis-server_1 ... done
		Creating app_node-app_1     ... done
		```

<br>&nbsp;
#### Accessing your application
<br>&nbsp;
At this point your terminal should see the redis and node server output on your screen. Now that our environment is up and running, we should be able to access the web page we built. In the terminal output, you should see the following output.

```
node-app_1      | Listening on port 8081
```

<br>&nbsp;
So theoretically, if you use the public DNS or IP of your instance and using the port 8081, you should be able to load the welcome page in your browser right?
``http://[YOUR_INSTACE_PUBLIC_IP_OR_DNS]:8081`` (ex: ``http://ec2-3-92-193-72.compute-1.amazonaws.com:8081``). However, if you try this now, you will get "Site Can't be reached" error.

This is failing because of port mapping we did earlier and AWS security groups. When we wrote docker-compose file, we instructed Docker to forward all traffic which comes to the 4001 port in the base machine to 8081 port in the container. So the correct port to use here is 4001. 

Even with port updated, ``http://ec2-3-92-193-72.compute-1.amazonaws.com:4001``, you will still get the same error. This is because we have not granted permission for port 4001 in our security group in AWS.

To grant access to the port,

1. Login to your AWS console and navigate to "EC2" service.
2. Go to you "Running instances" and click on the instance you are working with
3. In the details panel, Click on "Security" tab
4. Click on the security group (in my case it is "WebSecurity")
5. Click on "Edit Inbound Rules" and add the following rule
   ![Update Security Group]({{ site.BASE_PATH }}/assets/media/multi_container_app/Update_sec_group.png)
6. Click on "Save Rules" button.

<br>&nbsp;
#### Output
<br>
**Congratulations!!!** <br>
Now, if you type ``http://[YOUR_INSTACE_PUBLIC_IP_OR_DNS]:4001``, you should see the welcome page with the text **Hi visitor!, Number of visits are: 0**
<br>
Every time you refresh the page, you should see the count goes up by one. 

<br>&nbsp;
#### Bringing it all together
<br>&nbsp;
So finally, how does this all work?

![Figure 2 - Workflow]({{ site.BASE_PATH }}/assets/media/multi_container_app/Figure_2.png)